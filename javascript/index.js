var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
	return send({
		msgType: "join",
		data: {
			name: botName,
			key: botKey
		}
	});
});

function send(json) {
	client.write(JSON.stringify(json));
	return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {

	if (data.msgType === 'carPositions') {
		var state = data.data[0]
		var angle = Math.abs(state.angle);
		var position = state.piecePosition;
		var lap = position.lap;
		var speed = .5;

		if (angle < 10) // Pretty close to straightaway
		{
			speed += .4;
		} else if (angle > 30) // Turning... SLOW DOWN!
		{
			speed -= .2;
		} else {
			speed = .7; // Just drive normally you asshole!
		}

		send({
			msgType: "throttle",
			data: speed
		});

		console.log({
			speed: speed,
			angle: angle,
			piecedistance: position.inPieceDistance,
			position: position.pieceIndex,
			lap: lap
		});
	} else {
		if (data.msgType === 'join') {

			console.log('Joined')
		} else if (data.msgType === 'gameStart') {
			console.log('Race started');
			send({
				msgType: "throttle",
				data: 1.0
			});
			console.log(data.piecePosition)
		} else if (data.msgType === 'gameEnd') {
			console.log('Race ended');
		} else if (data.msgType === 'lapFinished') {

		}

		send({
			msgType: "ping",
			data: {}
		});
	}
});

jsonStream.on('error', function() {
	return console.log("disconnected");
});